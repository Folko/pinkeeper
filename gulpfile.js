var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var less = require('gulp-less');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var paths = {
  less: ['./less/**/*.less']
};

gulp.task('default', ['less']);

gulp.task('less', function(done) {

  var LessPluginIonic = require('less-plugin-ionic'),
  ionicless = new LessPluginIonic();

  var LessPluginAutoprefix = require('less-plugin-autoprefix'),
  autoprefix = new LessPluginAutoprefix({ browsers: ["Android >= 2.1","BlackBerry >= 7","Chrome >= 20","Firefox >= 21","Explorer >= 10","iOS >= 3.2","Opera > 12","Safari > 6","OperaMobile >= 12.1","ChromeAndroid >= 40","FirefoxAndroid >= 30","ExplorerMobile >= 10"] });

  gulp.src('./less/ionic.app.less')
    .pipe(less({
      plugins: [ionicless, autoprefix]
      }))
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.less, ['less']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});