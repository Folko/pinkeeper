var routing = require('./routing.js');
require('./app_modules/main/index.js');
require('./app_modules/slidebox/index.js');
require('../../lib/angular-local-storage/dist/angular-local-storage.min.js');

module.exports = angular.module('app', ['ionic', 'slideboxModule', 'mainModule', 'LocalStorageModule'])

.config(routing)

.run(['$ionicPlatform', function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
	  		cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});
}])

.config(function(localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('Pinkeeper')
        .setStorageType('localStorage')
        .setNotify(true, true)
})

angular.element(document).ready(function() {
    angular.bootstrap(document, ['app']);
});
