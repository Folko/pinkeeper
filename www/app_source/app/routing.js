module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider

    .state('app', {
        url: "/app",
        templateUrl: 'templates/menu.tpl.html',
        controller: 'mainCtrl'
    })
    .state('app.slidebox', {
        url: "/slidebox",
        views: {
        	menuContent: {
        		templateUrl: 'templates/slidebox.tpl.html',
                controller: 'slideboxCtrl'
        	}
        }
    })

    $urlRouterProvider.otherwise('/app/slidebox');
    
}]
