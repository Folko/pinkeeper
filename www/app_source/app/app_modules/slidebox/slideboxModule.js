var slideboxTpl = require('html-tpl!./templates/slidebox.tpl.html')();

module.exports = angular.module('slideboxModule', [])
.config([function() {

}])
.run(['$templateCache', function($templateCache) {

	$templateCache.put('templates/slidebox.tpl.html', slideboxTpl);

}])