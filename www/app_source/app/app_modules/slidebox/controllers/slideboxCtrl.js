require('../slideboxModule.js')

.controller('slideboxCtrl', ['$rootScope', '$scope', 'mainSrv', function($rootScope, $scope, mainSrv) {

	// screen Object
	//	*name: string
	//	*pinArr: Array

	// Инициализируе массив объектов с данными скринов, если в БД пусто, создаем пустой массив
	$scope.screenData = mainSrv.getScreenData();

	$rootScope.isFirstAdd = $scope.screenData.length > 0 ? false : true;

	// Вывод названия первого слайда при старте приложения
	$rootScope.screenName = $scope.screenData.length > 0 ? $scope.screenData[0].name : '';

	// Смена заголовков скринов при пролистывании
	$scope.slideHasChanged = function(index) {
		$rootScope.screenName = $scope.screenData[index].name;
	}
}]);