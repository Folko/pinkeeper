require('../mainModule.js')

.controller('mainCtrl', ['$rootScope', '$scope', '$ionicSideMenuDelegate', '$ionicModal', '$ionicSlideBoxDelegate', '$ionicPopup', 'mainSrv', function($rootScope, $scope, $ionicSideMenuDelegate, $ionicModal, $ionicSlideBoxDelegate, $ionicPopup, mainSrv) {
	
	// Всплывающие окна
	var editingPinPopup;
	var deletingPopup;
	var ratingPopup;

	$scope.screenData = mainSrv.getScreenData();

	$scope.rate = {
		val: mainSrv.getRating()
	}

	// First Edding pinscreen Flag
	$rootScope.isFirstAdd = true;

	// Флаг режима редактиорования
	$scope.isEditing = false;

	// Флаг состояния шаринга для меню
	$scope.isShare = false;

	// Form data for the login modal
	$scope.addData = {};

	// edited Pin value
	$scope.pinData = {
		val: 0
	}

	//ссылка на приложение в гугл плей
	$scope.appHref = 'https://play.google.com/store/apps/details?id=com.spyro23.pahomsays';

	// Включить состояние шаринга дял меню
	$scope.enableShare = function() {
		$scope.isShare = true;
	};
	// Выключить состояние шаринга дял меню
	$scope.disableShare = function() {
		$scope.isShare = false;
	};

	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('templates/add-pin.tpl.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});

	// Open Rate popup
	$scope.openRate = function() {
		ratingPopup = $ionicPopup.show({
			templateUrl: 'templates/rating-popup.tpl.html',
			title: 'Rate Pinkeeper',
			scope: $scope,
			buttons: [
				{
					text: 'Rate',
					type: 'button-positive',
					onTap: function() {   
			           return $scope.rate.val;
			        }
				},
				{ text: 'Cancel' }
			]
		});

		ratingPopup.then(function(data) {
			if (data != undefined) {
				mainSrv.setRating(data);
			}
		});
	}

	// Triggered in the login modal to close it
	$scope.closePin = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.addPin = function() {
		$scope.modal.show();
	};

	// Add Pinscreen
	$scope.doPin = function() {

		mainSrv.addScreenData($scope.addData.name)

		if ($rootScope.isFirstAdd == true) {
			$rootScope.screenName = $scope.screenData[0].name;
		}

		$ionicSlideBoxDelegate.$getByHandle('pin-viewer').update();

		$scope.closePin();
	};

	// Edit pinscreen
	$scope.editPin = function(screenIndex, pinIndex) {
		if($scope.isEditing == true) {
			console.log($scope.screenData);
			$scope.pinData.val = $scope.screenData[screenIndex].pinArr[pinIndex];
			editingPinPopup = $ionicPopup.show({
				templateUrl: 'templates/change-pin-popup.tpl.html',
				title: 'Enter Pin number',
				scope: $scope,
				buttons: [			
					{
						text: 'Save',
						type: 'button-positive',
						onTap: function() {   
				            return $scope.pinData.val;
				        }
					},
					{ text: 'Cancel' }
				]
			});

			editingPinPopup.then(function(data) {
				if (data != undefined) {
					mainSrv.editScreenData(screenIndex, pinIndex, $scope.pinData.val);
				}
			});
		}		
	}

	// Open delete popup
	$scope.deleteDialog = function() {
		deletingPopup = $ionicPopup.show({
			template: 'Are you sure you want delete ' + $rootScope.screenName +'?',
			title: 'Delete '+ $rootScope.screenName,
			scope: $scope,
			buttons: [
				{
					text: 'Delete',
					type: 'button-positive',
					onTap: function() {   
			            $scope.deleteScreen();
			        }
				},
				{ text: 'Cancel' }
			]
		});
	}

	// Delete Pinscreen
	$scope.deleteScreen = function() {
		var screenID = $ionicSlideBoxDelegate.$getByHandle('pin-viewer').currentIndex();

		mainSrv.deleteScreenData(screenID);
		
		$ionicSlideBoxDelegate.$getByHandle('pin-viewer').update();
		$ionicSlideBoxDelegate.$getByHandle('pin-viewer').slide(0);

		// TODO: костыль от бага ионик для удаления номера страницы при удалении не первого слайда

	}

	// Edit pinscreen/Enable Edit mode
	$scope.enableEdit = function() {
		$scope.isEditing = true;
		var screenID = $ionicSlideBoxDelegate.currentIndex();

		$ionicSlideBoxDelegate.enableSlide(false);
		$ionicSideMenuDelegate.toggleRight();
	}	

	// Disable edit mode
	$scope.applyEdit = function() {
		$scope.isEditing = false;
		$ionicSlideBoxDelegate.enableSlide(true);
		$ionicSideMenuDelegate.toggleRight();
	}


}]);