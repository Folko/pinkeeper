require('../mainModule.js')

.factory('mainSrv', ['localStorageService', function(localStorageService) {
	var screenData = [];
	var rating;

	function randomPins() {
		var randomArr = [];
		for (var i = 0; i < 16; i++) {
			randomArr[i] = Math.floor(Math.random()*10);
		};
		return randomArr;
	};

	// Запись данных о скринах
	// *field - string
	// *data - object, string, number
	function setData(field, data) {
		localStorageService.set(field, data);
	};

    return {

		// Сохранение данных о скринах
		addScreenData: function(name) {
			screenData.push({
				name: name,
				pinArr: randomPins()
			});
			console.log(screenData);
			setData('screenData', screenData);
		},

		// Получение данных о скринах
		getScreenData: function() {			
			if(screenData.length!=0) {
				return screenData;
			}
			screenData = localStorageService.get('screenData') || [];
			return screenData;
		},

		// Получение рейтинга
		getRating: function() {
			rating = localStorageService.get('rating') ? localStorageService.get('rating') : 1;
			return rating;
		},

		// Установка рейтинга
		setRating: function(ratingVal) {
			rating = ratingVal;
			setData('rating', rating);
		},

		// Редактирование пина в срине
		editScreenData: function(screenIndex, pinIndex, value) {
			screenData[screenIndex].pinArr[pinIndex] = value;
			setData('screenData', screenData);
		},

		// Удаление скрина
		deleteScreenData: function(id) {
			screenData.splice(id, 1);
			setData('screenData', screenData);
		},


    }
}]);