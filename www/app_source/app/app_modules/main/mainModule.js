var addPinTpl = require('html-tpl!./templates/add-pin.tpl.html')();
var menuTpl = require('html-tpl!./templates/menu.tpl.html')();
var changePinPopupTpl = require('html-tpl!./templates/change-pin-popup.tpl.html')();
var ratingPopupTpl = require('html-tpl!./templates/rating-popup.tpl.html')();


module.exports = angular.module('mainModule', [])
.config([function() {

}])
.run(['$templateCache', function($templateCache) {
	
	$templateCache.put('templates/add-pin.tpl.html', addPinTpl);
	$templateCache.put('templates/menu.tpl.html', menuTpl);
	$templateCache.put('templates/change-pin-popup.tpl.html', changePinPopupTpl);
	$templateCache.put('templates/rating-popup.tpl.html', ratingPopupTpl);

}])