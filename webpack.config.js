var ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    entry: {
        app: "./www/app_source/app/index.js",
    },
    output: {
        path: __dirname,
        filename: "./www/js/[name].js"
    },

    module: {
        loaders: [{
            test: "\.tpl.html$/",
            loader: "html-tpl"
        }]
    },
};